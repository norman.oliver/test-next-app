import React from 'react'
import Link from 'next/link'

export default function Home() {
  return (
    <main className='flex flex-col items-center justify-center min-h-screen'>
      <h1 className='text-2xl font-bold mb-4'>Welcome to Homepage</h1>
      <Link href='/AddEmployee'>
        <button className='bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'>
          Add an Employee
        </button>
      </Link>
    </main>
  )
}
