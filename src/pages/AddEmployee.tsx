import React, { useState } from 'react'
import Layout from '../components/Layout'
import { QuestionMarkCircleIcon } from '@heroicons/react/solid'

const AddEmployee: React.FC = () => {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    // Add logic to handle form submission
  }

  return (
    <Layout>
      <div className='w-full h-screen flex items-center justify-center'>
        <div className='border border-black rounded-md overflow-hidden'>
          <div className='bg-white py-5'>
            <h1 className=' text-gray-500 text-xl font-semibold text-center'>
              Add Employee
            </h1>
          </div>
          <div className='border-t border-gray-200 px-4 bg-gray-100'>
            <p className='text-xs text-blue-500 pt-6 pb-1 text-left'>
              Important fields - Mandatory Fields Are Marked With{' '}
              <span className='text-red-500'>*</span>
            </p>

            <form onSubmit={handleSubmit}>
              <div className='flex justify-between space-x-6'>
                <label className='block text-gray-500 w-48 text-sm font-bold'>
                  First Name(s)<span className='text-red-500'>*</span>{' '}
                  <QuestionMarkCircleIcon className='h-3 w-3 inline' />
                  <input className='w-full px-2 py-1 border' type='text' />
                </label>
                <label className='block text-gray-500 w-48 text-sm font-bold'>
                  Last Name<span className='text-red-500'>*</span>{' '}
                  <QuestionMarkCircleIcon className='h-3 w-3 inline' />
                  <input className='w-full px-2 py-1 border' type='text' />
                </label>
              </div>
              <label className='block text-gray-500 w-48 text-sm font-bold mt-2'>
                Preferred Name<span className='text-red-500'>*</span>{' '}
                <QuestionMarkCircleIcon className='h-3 w-3 inline' />
                <input className='w-full px-2 py-1 border' type='text' />
              </label>
              <div className='flex justify-between space-x-6 mt-2'>
                <label className='block text-gray-500 w-48 text-sm font-bold'>
                  Employee Code<span className='text-red-500'>*</span>{' '}
                  <QuestionMarkCircleIcon className='h-3 w-3 inline' />
                  <input className='w-full px-2 py-1 border' type='text' />
                </label>
                <label className='block text-gray-500 w-48 text-sm font-bold'>
                  Gender<span className='text-red-500'>*</span>{' '}
                  <QuestionMarkCircleIcon className='h-3 w-3 inline' />
                  <select className='w-full px-2 py-1 border' type='text'>
                    <option value=''></option>
                  </select>
                </label>
              </div>
              <button
                className='block w-12 h-9 bg-green-600 text-white rounded ml-auto my-4'
                type='submit'
              >
                Add
              </button>
            </form>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default AddEmployee
