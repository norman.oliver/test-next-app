import '@/app/globals.css'
import { AppProps } from 'next/app'
import { Inter } from 'next/font/google'
import 'tailwindcss/tailwind.css'
import Head from 'next/head'
import { SSRProvider } from '@react-aria/ssr'

const inter = Inter({ subsets: ['latin'] })

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>Employee Management System</title>
        <meta name='viewport' content='initial-scale=1.0, width=device-width' />
      </Head>

      <SSRProvider>
        <div className={inter.className}>
          <main>
            <Component {...pageProps} />
          </main>
        </div>
      </SSRProvider>
    </>
  )
}

export default MyApp
