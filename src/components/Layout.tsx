import Link from 'next/link'

interface LayoutProps {
  children: React.ReactNode
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <div className='bg-gray-300'>
      <nav className='bg-white py-4'>
        <div className='container mx-auto px-4'>
          <ul className='flex'>
            <li className='mr-6'>
              <Link href='/'>
                <button className='text-gray-800'>Home</button>
              </Link>
            </li>
          </ul>
        </div>
      </nav>
      <div className='container mx-auto px-4 py-8'>{children}</div>
    </div>
  )
}

export default Layout
